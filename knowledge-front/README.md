# Quasar App

> A Quasar project

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:8080
$ quasar dev

# build for production with minification
$ quasar build

# lint code
$ quasar lint
```

## Database

Run the server after execute the dependencies install.

```bash
# install dependencies
$ npm install

# copy the example db to your db
$ cp db.json.template db.json

# run mock db server
$ ./node_modules/json-server/bin/index.js db.json

# to access the data, use your browser or curl
# the example bellow is using the firefox
$ firefox www.localhost:3000/
```

In file db.json has only two keys `user` and `registers`. This two registers is
the indication to json-server use that as collections of database.

The current db.json file has an example of user with a empty records.

