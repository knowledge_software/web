# Knowledge

The knowledge is the base of actual society and we need to learn how use the
computer to help us managing and recording our knowledge.

# Project

## Frontend

Is a application running in [Quasar](http://quasar-framework.org/).

### Features proposal:

  - Save n types of contents
    - IMAGES
    - VÍDEOS
    - DOCUMENTS

  - Manager your profile

  - Todo of contents to study

## Command Line Application

Is a simple application in command line to manage the contents

### Features proposal:

  - Todo of contents
  - Organize the content in persistence
  - Manage the content to add
